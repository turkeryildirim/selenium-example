import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class BaseTest {
    public WebDriver driver;
    public WebDriverWait wait;

    @Before
    public void setup() {
        //Create a Firefox driver. All test and page classes use this driver.
        driver = new ChromeDriver();

        //Create a wait. All test and page classes use this wait.
        wait = new WebDriverWait(driver, 15);

        //Maximize Window
        driver.manage().window().maximize();
    }

    @After
    public void teardown() {
        driver.quit();
    }
}