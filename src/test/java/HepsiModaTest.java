import org.junit.Test;

import com.example.hepsimoda.HepsiModa;

public class HepsiModaTest extends BaseTest {

    @Test
    public void Test() {

        HepsiModa hepsiModa = new HepsiModa(driver, wait);

        //PAGE METHODS
        hepsiModa.goToHome();
        hepsiModa.goToCategory();
        hepsiModa.goToFirst();
        hepsiModa.addBasket();
        hepsiModa.continueCheckout();
        hepsiModa.login("", "");
    }
}