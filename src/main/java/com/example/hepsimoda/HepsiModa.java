package com.example.hepsimoda;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class HepsiModa extends BasePage {

    //Constructor
    public HepsiModa(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
    }

    //Page Variables
    String baseURL = "https://www.hepsiburada.com/";

    //Web Elements
    String hepsimoda = "//*[@id=\"hepsimoda\"]";
    String kolSaatleri = "//a[@data-title=\"Kol Saatleri\"]";
    String firstItem = "/html/body/div[3]/main/div[4]/div/div/div/div[2]/section/div[7]/div[3]/div/div/div/div/ul/li[1]/div/a";
    String basketButton = "//*[@id=\"addToCart\"]";
    String continueCheckout = "/html/body/div/div[2]/div/div[1]/aside/section/div/div[1]/div[2]/button";

    String checkBox = "/html/body/div[2]/div[2]/div/section[1]/div/div[1]/div[1]/div/label";
    String emailLocation = "//*[@id=\"email\"]";
    String passwordLocation = "//*[@id=\"password\"]";

    String delivery = "//*[@id=\"delivery-container\"]";

    //Page Methods

    //Go to Homepage
    public void goToHome() {
        driver.get(baseURL);
    }

    //Go to Category
    public void goToCategory() {
        moveToElement(By.xpath(hepsimoda));
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath(kolSaatleri)));
        click(By.xpath(kolSaatleri));
    }

    //Go to First Item
    public void goToFirst() {
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath(firstItem)));
        click(By.xpath(firstItem));
    }

    //Adding Item to Basket
    public void addBasket() {
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath(basketButton)));
        click(By.xpath(basketButton));
    }

    //Continue Checkout Page
    public void continueCheckout() {
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath(continueCheckout)));
        click(By.xpath(continueCheckout));
    }

    //Login
    public void login(String email, String password) {
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath(checkBox)));
        click(By.xpath(checkBox));
        writeText(By.xpath(emailLocation), email);
        writeText(By.xpath(passwordLocation), password);
        submit(By.className("btn-login-submit"));
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(delivery)));
        Assert.assertTrue("Page can not loading.", driver.getTitle().contains("Teslimat Bilgileri"));
    }
}
